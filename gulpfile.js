var gulp = require('gulp'),
	gutil = require('gulp-util'),
	sass = require('gulp-sass'),
	browserSync = require('browser-sync'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	cleancss = require('gulp-clean-css'),
	rename = require('gulp-rename'),
	autoprefixer = require('gulp-autoprefixer'),
	notify = require("gulp-notify"),
	rsync = require('gulp-rsync'),
	del = require('del'),
	imagemin = require('gulp-imagemin'),
	cache = require('gulp-cache'),
	ftp = require('vinyl-ftp'),
	fileinclude = require('gulp-file-include'),
	postcss = require('gulp-postcss'),
	mqpacker = require('css-mqpacker'),
	syntax = 'scss',
	lost = require('lost');


gulp.task('browser-sync', function () {
	browserSync({
		injectChanges: true,
		server: {
			baseDir: 'app'
		},
		notify: false,
		open: false,
		// online: false, // Work Offline Without Internet Connection
		// tunnel: true, tunnel: "projectname", // Demonstration page: http://projectname.localtunnel.me
	})
});

gulp.task('html', function () {
	gulp.src('app/html/*.html')
		.pipe(fileinclude({
			prefix: '@@',
			basepath: '@file'
		}))
		.pipe(gulp.dest('app'))
		.pipe(browserSync.stream())
});


const sortCSSmq = require('sort-css-media-queries');

var plugins = [
	// mqpacker({
	// 	sort: sortCSSmq.desktopFirst
	// });
	lost(),
];

gulp.task('styles', function () {
	return gulp.src('app/' + syntax + '/**/*.' + syntax + '')
		.pipe(sass({
			outputStyle: 'expanded'
		}).on("error", notify.onError())) //Values: nested, expanded, compact, compressed
		.pipe(autoprefixer(['last 4 versions']))
		.pipe(postcss(plugins))
		.pipe(gulp.dest('app/css'))

		.pipe(cleancss({
			level: {
				1: {
					specialComments: 0
				}
			}
		})) // Opt., comment out when debugging
		.pipe(rename({
			suffix: '.min',
			prefix: ''
		}))
		.pipe(gulp.dest('app/css'))
		.pipe(browserSync.stream())
});

gulp.task('js', function () {
	return gulp.src('app/js/common.js') // Always at the end
		.pipe(rename('scripts.js'))
		.pipe(gulp.dest('app/js'))

		.pipe(rename({
			suffix: '.min',
			prefix: ''
		}))
		.pipe(uglify()) // Mifify js (opt.)
		.pipe(gulp.dest('app/js'))

		//.pipe(browserSync.reload({ stream: true }))
		.pipe(browserSync.stream())
});

// gulp.task('rsync', function() {
// return gulp.src('app/**')
// .pipe(rsync({
// root: 'app/',
// hostname: 'username@yousite.com',
// destination: 'yousite/public_html/',
// //include: ['*.htaccess'], // Includes files to deploy
// exclude: ['**/Thumbs.db', '**/*.DS_Store'], // Excludes files from deploy
// recursive: true,
// archive: true,
// silent: false,
// compress: true
// }))
// });

gulp.task('watch', ['styles', 'js', 'browser-sync'], function () {
	gulp.watch('app/' + syntax + '/**/*.' + syntax + '', ['styles']);
	gulp.watch(['app/js/common.js'], ['js']);
	gulp.watch('app/html/**/*.html', ['html']);
	gulp.watch('app/*.html', browserSync.reload)
	gulp.watch('app/' + syntax + '/**/*.' + syntax + '', browserSync.reload)
});

gulp.task('imagemin', function () {
	return gulp.src(['app/img/**/*', '!app/img/**/*.svg']) //exclude svg
		.pipe(cache(imagemin())) // Cache Images
		.pipe(gulp.dest('dist/img'));
});

gulp.task('build', ['removedist', 'imagemin', 'styles', 'js'], function () {
	var buildFiles = gulp.src([
		'app/*.html',
		'app/.htaccess',
	]).pipe(gulp.dest('dist'));

	var buildCss = gulp.src([
		'app/css/*.css',
	]).pipe(gulp.dest('dist/css'));

	var buildJs = gulp.src([
		'app/js/scripts.min.js',
		'app/js/scripts.js',
	]).pipe(gulp.dest('dist/js'));

	var buildFonts = gulp.src([
		'app/fonts/**/*',
	]).pipe(gulp.dest('dist/fonts'));

	var buildLibs = gulp.src([
		'app/libs/**/*',
	]).pipe(gulp.dest('dist/libs'));

	var buildFiles = gulp.src([
		'app/img/**/*.svg',
	]).pipe(gulp.dest('dist/img'));

});

gulp.task('default', ['watch']);
gulp.task('removedist', function () {
	return del.sync('dist');
});
gulp.task('clearcache', function () {
	return cache.clearAll();
});