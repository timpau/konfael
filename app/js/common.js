$(function () {
	svg4everybody();
	$(window).enllax();

	$(window).scroll(function () {
		var sc = $(window).scrollTop()
		var wd = $(window).width()
		if (sc > 100) {
			$("header").addClass("fixed-header")
			if (wd > 991) {
				$('.first__menu').removeClass('order-first');
			}
		} else {
			$("header").removeClass("fixed-header")
			if (wd > 991) {
				$('.first__menu').addClass('order-first');
			}
		}
	});

	$('.hamburger__menu').on('click', function (e) {
		e.preventDefault();
		$('body').addClass('lock');
		$('.footer__menu--wrap').addClass('mobile-menu').prepend('<a href="#" class="close__menu"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="./img/sprite.svg#hamburger-close"></use></svg></a>')
	});

	$('body').on('click', '.close__menu', function (e) {
		e.preventDefault();
		$('body').removeClass('lock');
		$('.footer__menu--wrap').removeClass('mobile-menu')
		$('.close__menu').remove();;
	});

	$('.calculator').on('click', '.label__tab', function () {
		$link = $(this).data('href');
		$parent = $(this).closest('.calculator');

		$parent.find('.tab-panel').hide();
		$parent.find($link).show();
		$('.program--additional').slick('refresh');

	});

	function change_name() {
		$btn_dropdown = $('#dropdownMenuButton');
		$input = $('[name="type-prog"]:checked');
		$name = $input.next().text();

		$btn_dropdown.text($name);
	}
	change_name();
	$('[name="type-prog"]').on('change', function () {
		change_name();
	})

	$('#foto-slide').slick({
		dots: false,
		infinite: true,
		slidesToShow: 1,
		centerMode: true,
		centerPadding: '20px',
		variableWidth: true,
		nextArrow: '<button class="slide-next slick-arrow" aria-label="Next" type="button"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="./img/sprite.svg#arrow-right"></use></svg></button>',
		prevArrow: '<button class="slide-prev slick-arrow" aria-label="Previous" type="button"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="./img/sprite.svg#arrow-left"></use></svg></button>',
		responsive: [{
			breakpoint: 576,
			settings: {
				centerPadding: '100px',
			},
		}, ]
	});

	$('#insta-slide').slick({
		dots: false,
		infinite: true,
		slidesToShow: 1,
		centerMode: true,
		variableWidth: true,
		nextArrow: '<button class="slide-next slick-arrow" aria-label="Next" type="button"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="./img/sprite.svg#arrow-right"></use></svg></button>',
		prevArrow: '<button class="slide-prev slick-arrow" aria-label="Previous" type="button"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="./img/sprite.svg#arrow-left"></use></svg></button>',
	});

	$('#sale-slider').slick({
		dots: true,
		infinite: true,
		slidesToShow: 1,
		nextArrow: '<button class="slide-next slick-arrow" aria-label="Next" type="button"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="./img/sprite.svg#arrow-right"></use></svg></button>',
		prevArrow: '<button class="slide-prev slick-arrow" aria-label="Previous" type="button"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="./img/sprite.svg#arrow-left"></use></svg></button>',
	});

	$('#review-slide').slick({
		// arrows: false,
		dots: true,
		draggable: false,
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 3,
		rows: 1,
		nextArrow: '<button class="slick-next slick-arrow" aria-label="Next" type="button"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="./img/sprite.svg#arrow-right"></use></svg></button>',
		prevArrow: '<button class="slick-prev slick-arrow" aria-label="Previous" type="button"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="./img/sprite.svg#arrow-left"></use></svg></button>',
		appendDots: $(".review-slide__nav"),
		appendArrows: $(".review-slide__nav"),
		responsive: [{
				breakpoint: 992,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
				},
			},
			{
				breakpoint: 576,
				settings: {
					// slidesPerRow: 1,
					// rows: 3,
					slidesToShow: 1,
					slidesToScroll: 1,
				},
			},
		],
	});

	$('.program--additional').slick({
		arrows: true,
		dots: false,
		infinite: false,
		slidesToShow: 4,
		slidesToScroll: 1,
		nextArrow: '<button class="slide-next slick-arrow" aria-label="Next" type="button"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="./img/sprite.svg#arrow-right"></use></svg></button>',
		prevArrow: '<button class="slide-prev slick-arrow" aria-label="Previous" type="button"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="./img/sprite.svg#arrow-left"></use></svg></button>',
		responsive: [{
				breakpoint: 992,
				settings: {
					slidesToShow: 3,
				},
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 2,
				},
			},
			{
				breakpoint: 416,
				settings: {
					slidesToShow: 1,
				},
			},
		],
	});

	$('#home-slider').slick({
		dots: false,
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
		nextArrow: '<button class="slide-next slick-arrow" aria-label="Next" type="button"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="./img/sprite.svg#arrow-right"></use></svg></button>',
		prevArrow: '<button class="slide-prev slick-arrow" aria-label="Previous" type="button"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="./img/sprite.svg#arrow-left"></use></svg></button>',
	});

	$service_slider = $('#service-slider');
	settings = {
		arrows: true,
		nextArrow: '<button class="slide-next slick-arrow" aria-label="Next" type="button"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="./img/sprite.svg#arrow-right"></use></svg></button>',
		prevArrow: '<button class="slide-prev slick-arrow" aria-label="Previous" type="button"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="./img/sprite.svg#arrow-left"></use></svg></button>',
		responsive: [{
				breakpoint: 9999,
				settings: "unslick"
			},
			{
				breakpoint: 1200,
				settings: {
					slidesToShow: 5
				},
			},
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 4,
				},
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 3,
				},
			},
			{
				breakpoint: 576,
				settings: {
					slidesToShow: 2,
				},
			},
			{
				breakpoint: 376,
				settings: {
					slidesToShow: 1,
				},
			},
		],
	};
	$service_slider.slick(settings);

	$(window).on('resize', function () {
		if ($(window).width() > 1200) {
			if ($service_slider.hasClass('slick-initialized'))
				$service_slider.slick('unslick');
			return
		}
		if (!$service_slider.hasClass('slick-initialized'))
			return $service_slider.slick(settings);
	});


	//input range style
	var s = document.createElement('style'),
		r = document.querySelector('[type=range]');
	document.body.appendChild(s);

	if (typeof (r) != 'undefined' && r != null) {
		s.textContent = '.js input[type=range]::-webkit-slider-runnable-track{background-size: ' + r.value + '% 100%}' + '.js input[type=range]::-moz-range-track{background-size: ' + r.value + '% 100%}';
		s.textContent += '.js input[type=range] /deep/ #thumb:before{content:"' + r.value + '%"}';

		/* IE doesn't need the JS part & this won't work in IE anyway ;) */
		r.addEventListener('input', function () {
			var val = this.value + '% 100%';

			s.textContent = '.js input[type=range]::-webkit-slider-runnable-track{background-size:' + val + '}' + '.js input[type=range]::-moz-range-track{background-size:' + val + '}';
			s.textContent += '.js input[type=range] /deep/ #thumb:before{content:"' + this.value + '%"}';
		}, false);
	}

	ymaps.ready(function () {

		if ($('#map').length) {
			var myMap = new ymaps.Map('map', {
				center: [55.819581, 37.348153],
				zoom: 16,
				controls: []
			}, {});
			myPlacemark = new ymaps.Placemark(myMap.getCenter(), {}, {
				iconLayout: 'default#image',
				iconImageHref: './img/pointer.png',
				iconImageSize: [65, 80],
				iconImageOffset: [-33, -65]
			});
			myMap.geoObjects.add(myPlacemark);
		}
		if ($('#map2').length) {
			var myMap2 = new ymaps.Map('map2', {
				center: [55.750445, 37.427537],
				zoom: 16,
				controls: []
			}, {});
			myPlacemark2 = new ymaps.Placemark(myMap2.getCenter(), {}, {
				iconLayout: 'default#image',
				iconImageHref: './img/pointer2.png',
				iconImageSize: [65, 80],
				iconImageOffset: [-33, -65]
			});
			myMap2.geoObjects.add(myPlacemark2);
		}
		if ($('#map3').length) {
			var myMap3 = new ymaps.Map('map3', {
				center: [55.807749, 37.570540],
				zoom: 16,
				controls: []
			}, {});
			myPlacemark3 = new ymaps.Placemark(myMap3.getCenter(), {}, {
				iconLayout: 'default#image',
				iconImageHref: './img/pointer.png',
				iconImageSize: [65, 80],
				iconImageOffset: [-33, -65]
			});
			myMap3.geoObjects.add(myPlacemark3);
		}

	});

});


// Mask Phone
$("input[type='tel'").mask('+7 000 000-00-00');